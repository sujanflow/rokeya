//
//  EditProfileViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQDropDownTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *contactNumberTextFiewld;
@property (weak, nonatomic) IBOutlet UITextField *emergencyContactTextField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *relationTextField;
@property (weak, nonatomic) IBOutlet UITextField *presentAddress;

@property (weak,nonatomic) NSMutableDictionary *userInfo;

@end

NS_ASSUME_NONNULL_END
