//
//  main.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
