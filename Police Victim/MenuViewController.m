//
//  MenuViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "MenuViewController.h"
#import "KYDrawerController.h"
#import "HomeViewController.h"
#import "HexColors.h"
#import "ProfileViewController.h"
#import "UIView+Utils.h"

@interface MenuViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSArray* menuArray;
    NSArray* menuImageArray;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    menuArray = [NSArray arrayWithObjects: @"Help Menu", @"Profile", @"Histories", @"About", nil];
    menuImageArray = [NSArray arrayWithObjects: @"tutorial_icon", @"shopping_icon", @"rateUs_icon", @"feedback_icon", nil];
    
    
    _menutableView.delegate = self;
    _menutableView.dataSource = self;
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Index"];
    //
    
    [[ServerManager sharedManager] getCurrentProfileDetails:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"resultDataDic %@",resultDataDic);
        
        self.userName.text = [NSString stringWithFormat:@"%@ %@",[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_first_name"],[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_last_name"]];
        self.phoneNoLabel.text = [NSString stringWithFormat:@"%@",[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_contact_number"]];


    }];
    //
    
    [self.view setBackGroundGradientFromColor:[UIColor hx_colorWithHexString:@"1D5178"] toColor:[UIColor hx_colorWithHexString:@"0D3874"]];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"viewDidAppear in drawer %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"Index"]);
    
    int selectedIndex = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Index"]intValue];
    
    [self.menutableView selectRowAtIndexPath:([NSIndexPath indexPathForRow:selectedIndex inSection:0]) animated:NO scrollPosition: UITableViewScrollPositionNone];
    
    [self.menutableView reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return menuArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    UIImageView *menuIcon = (UIImageView*)[cell viewWithTag:201];
    menuIcon.image = [UIImage imageNamed:[menuImageArray objectAtIndex:indexPath.row]];
    
    UILabel *menuTitle = (UILabel*)[cell viewWithTag:202];
    menuTitle.text = [menuArray objectAtIndex:indexPath.row];
  //  menuTitle.textColor = [UIColor blackColor];
    cell.contentView.backgroundColor = UIColor.clearColor;

    int selectedIndex = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Index"]intValue];
    
    if (indexPath.row == selectedIndex) {
        
        cell.selected = YES;
        
        if (cell.selected) {
            
            //menuTitle.textColor = UIColor.whiteColor;
            cell.contentView.backgroundColor = [UIColor hx_colorWithHexString:@"160F41"];
        }

    }
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath{
//    [tableView deselectRowAtIndexPath:newIndexPath animated:YES];
//
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    
    
     UIViewController *viewController;
    
    switch ([newIndexPath row]) {
        case 0:{
            
            viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];

            break;
        }
            
        case 1:{
            
            viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
       
            break;
        }
        case 2:{
            
             viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HistoryViewController"];
            
            break;
        }
        case 3:{
            
           viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutViewController"];
            break;
        }
            
            
        default:{
            
            break;
        }
            
            
            
            
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:newIndexPath.row forKey:@"Index"];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:newIndexPath];
    UILabel *menuTitle = (UILabel*)[cell viewWithTag:202];
    // menuTitle.textColor = [UIColor hx_colorWithHexString:@"1E82F0"];
    cell.contentView.backgroundColor = [UIColor hx_colorWithHexString:@"160F41"];

    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:viewController];
    
    navController.navigationBar.hidden = YES;
    elDrawer.mainViewController=navController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:YES];
    
   
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"didDeselectRowAtIndexPath");
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *menuTitle = (UILabel*)[cell viewWithTag:202];
  //  menuTitle.textColor = [UIColor hx_colorWithHexString:@"1E82F0"];
    cell.contentView.backgroundColor = UIColor.clearColor;

}



@end
