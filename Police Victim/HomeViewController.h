//
//  HomeViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *designationLabel;


@end

NS_ASSUME_NONNULL_END
