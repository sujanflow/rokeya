//
//  HistoryViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lastActivatedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastUpdatedTimeLabel;

@property (weak, nonatomic) IBOutlet UITableView *historyTableView;
@property (weak, nonatomic) IBOutlet UIView *timeView;


@end

NS_ASSUME_NONNULL_END
