//
//  CALayer+Utils.h
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 1/18/16.
//  Copyright © 2016 Niteco. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Utils)
- (NSArray *)allSubLayers;
@end
