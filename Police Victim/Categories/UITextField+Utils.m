//
//  UITextField+Utils.m
//  UberGoods Customer
//
//  Created by Tanvir Palash on 30/6/18.
//  Copyright © 2018 Tanvir Palash. All rights reserved.
//

#import "UITextField+Utils.h"
#import "HexColors.h"
#import "AppConstant.h"

@implementation UITextField (Utils)

- (void)makeCircleForBorderWidth:(CGFloat)width
{
    self.layer.borderColor = [HXColor hx_colorWithHexString:textFieldBorder].CGColor;
    self.layer.cornerRadius = 0.5 * self.bounds.size.height;
    self.layer.borderWidth = width;
    self.clipsToBounds = true;
}

- (void)makeRound
{
    self.layer.cornerRadius = 15;
    self.clipsToBounds = true;
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [HXColor hx_colorWithHexString:textFieldBorder].CGColor;

    
}

- (void)setCustomTextRect
{
        self.layer.sublayerTransform = CATransform3DMakeTranslation(15, 0, 0);
        self.textAlignment=NSTextAlignmentLeft;
}

-(void)setRightPaddingView:(CGFloat)amount
{
    UIView* paddingView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, amount, self.frame.size.height)];
    self.rightView = paddingView;
    self.rightViewMode =UITextFieldViewModeAlways;
}

- (void)setPlaceholder:(NSString*)text withColor:(UIColor*)color
{
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: color}];

}


@end
