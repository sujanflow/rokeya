//
//  MenuViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *menutableView;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *phoneNoLabel;


@end

NS_ASSUME_NONNULL_END
