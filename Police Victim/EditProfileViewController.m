//
//  EditProfileViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ServerManager.h"

@interface EditProfileViewController ()<IQDropDownTextFieldDelegate>

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.relationTextField.showDismissToolbar = YES;
    [self.relationTextField setItemList:[NSArray arrayWithObjects:@"Father",@"Mother",@"Brother",@"Sister",@"Husband",@"Grandparent",@"Uncle",@"Aunt",@"Legal Guardian", nil]];
    [self.relationTextField setItemListUI:[NSArray arrayWithObjects:@"Father",@"Mother",@"Brother",@"Sister",@"Husband",@"Grandparent",@"Uncle",@"Aunt",@"Legal Guardian", nil]];
    
    
    self.contactNumberTextFiewld.text = [self.userInfo objectForKey:@"victim_occupation"];
    self.emergencyContactTextField.text = [self.userInfo objectForKey:@"victim_emergency_contact"] ;
    self.relationTextField.text = [self.userInfo objectForKey:@"victim_emergency_contact_relation"];
    self.presentAddress.text = [self.userInfo objectForKey:@"victim_present_address"];

}

-(void)textField:(nonnull IQDropDownTextField*)textField didSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
}
-(BOOL)textField:(nonnull IQDropDownTextField*)textField canSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return YES;
}

-(IQProposedSelection)textField:(nonnull IQDropDownTextField*)textField proposedSelectionModeForItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return IQProposedSelectionBoth;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)doneClicked:(UIBarButtonItem*)button
{
    [self.view endEditing:YES];
    
    NSLog(@"textFieldTextPicker.selectedItem: %@", self.relationTextField.selectedItem);
    
}

- (IBAction)updateButton:(id)sender {
    
    
     NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    
    [userInfo setObject:self.presentAddress.text forKey:@"present_address"];
    [userInfo setObject:self.contactNumberTextFiewld.text forKey:@"victim_occupation"];
    [userInfo setObject:self.emergencyContactTextField.text forKey:@"victim_emergency_contact"];
    [userInfo setObject:self.relationTextField.text forKey:@"emergency_contact_relation"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *newString = [[newStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    
    NSLog(@"tempString %@",newString);
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    [param setObject:newString forKey:@"update"];

    
    [[ServerManager sharedManager]updateCurrentProfileDetailswith:param withBlock:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"????%@",resultDataDic);
        
    }];
}



- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
