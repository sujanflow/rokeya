//
//  AppDelegate.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationTracker.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic) NSTimer* locationUpdateTimer;
@property LocationTracker * locationTracker;
@property LocationShareModel * locationShareModel;

@end

