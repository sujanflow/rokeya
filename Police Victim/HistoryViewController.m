//
//  HistoryViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "HistoryViewController.h"
#import "KYDrawerController.h"
#import "ServerManager.h"

@interface HistoryViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    
    NSMutableArray *helpArray;
    
}

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _historyTableView.delegate = self;
    _historyTableView.dataSource = self;
    
    
    [[ServerManager sharedManager] postHistory:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"History %@",resultDataDic);
        
       
        self.lastActivatedTimeLabel.text = [NSString stringWithFormat:@"%@",[[resultDataDic  objectForKey:@"histories"] objectForKey:@"last_session_activated"] ];
        self.lastUpdatedTimeLabel.text = [NSString stringWithFormat:@"%@",[[resultDataDic  objectForKey:@"histories"] objectForKey:@"last_session_updated"]];

        
        self->helpArray = [[resultDataDic  objectForKey:@"histories"] objectForKey:@"cases"];
        
        [self.historyTableView reloadData];
        
    }];
    
    _historyTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    self.timeView.layer.borderWidth = 1;
    self.timeView.layer.borderColor = [UIColor hx_colorWithHexString:@"160F41"].CGColor;
    self.timeView.layer.cornerRadius = 10;
    
    self.historyTableView.layer.borderWidth = 1;
    self.historyTableView.layer.borderColor = [UIColor hx_colorWithHexString:@"160F41"].CGColor;
    self.historyTableView.layer.cornerRadius = 10;
    
    
    }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return helpArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"historyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    NSDictionary* temp = [helpArray objectAtIndex:indexPath.row];

    UILabel *timeLabel = (UILabel*)[cell viewWithTag:301];
    timeLabel.text = [NSString stringWithFormat:@"Time: %@",[temp objectForKey:@"filed_at"]];
    
    UILabel *placeLabel = (UILabel*)[cell viewWithTag:302];
    placeLabel.text = [NSString stringWithFormat:@"Place: %@",[temp objectForKey:@"crime_location"]];
    

    
    return cell;
    
}
- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

@end
