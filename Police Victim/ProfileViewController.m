//
//  ProfileViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "ProfileViewController.h"
#import "KYDrawerController.h"
#import "EditProfileViewController.h"
#import "ServerManager.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface ProfileViewController (){
    
    NSMutableDictionary *userInfo;
    
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"lalvnslkndsfl");
}

- (void)viewWillAppear:(BOOL)animated{
    
    [[ServerManager sharedManager] getCurrentProfileDetails:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        NSLog(@"resultDataDic %@",resultDataDic);
        
        self->userInfo = [resultDataDic objectForKey:@"message"];
        
        
        self.firstname.text = [NSString stringWithFormat:@"First name: %@",[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_first_name"]];
        self.lastName.text =[NSString stringWithFormat:@"Last name: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_last_name"]];
        self.age.text =[NSString stringWithFormat:@"Age: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_age"]];
        self.contactNo.text = [NSString stringWithFormat:@"Contact Number: %@",[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_contact_number"]];
        self.emergencyContact.text = [NSString stringWithFormat:@"Emergency Contact: %@",[[resultDataDic objectForKey:@"message"]objectForKey:@"victim_emergency_contact"]];
        self.emerContactRelation.text =[NSString stringWithFormat:@"Emergency Contact Relation: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_emergency_contact_relation"]];
        self.parmanentAddress.text =[NSString stringWithFormat:@"Permanent Address: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_permanent_address"]];
        self.presentAddress.text =[NSString stringWithFormat:@"Present Address: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_present_address"]];
        self.occupation.text =[NSString stringWithFormat:@"Occupation: %@", [[resultDataDic objectForKey:@"message"]objectForKey:@"victim_occupation"]];
        
        
    }];
}

- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

- (IBAction)editbuttonAction:(id)sender {
    
    EditProfileViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    
    viewController.userInfo = userInfo;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (IBAction)logoutButtonAction:(id)sender {
    
    [[ServerManager sharedManager] logOut:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        if (success) {
            
            NSLog(@"logOut");
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"isLoggedIn"];
            
            
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            LoginViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"LoginViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginVC];
            navController.navigationBar.hidden = YES;
            
            UIWindow *window = [UIApplication sharedApplication].delegate.window;
            window.rootViewController = navController;
            
        }
    }];
}


@end
