//
//  ProfileViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 19/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *firstname;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *occupation;
@property (weak, nonatomic) IBOutlet UITextField *emergencyContact;
@property (weak, nonatomic) IBOutlet UITextField *contactNo;
@property (weak, nonatomic) IBOutlet UITextField *parmanentAddress;
@property (weak, nonatomic) IBOutlet UITextField *presentAddress;
@property (weak, nonatomic) IBOutlet UITextField *emerContactRelation;



@end

NS_ASSUME_NONNULL_END
