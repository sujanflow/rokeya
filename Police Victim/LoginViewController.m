//
//  LoginViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 1/4/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "LoginViewController.h"
#import "ServerManager.h"
#import "KYDrawerController.h"
#import "HexColors.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.phoneNoTextField.text = @"+8801514514914";
    self.passwordTextField.text = @"victim25";
    
    [self.navigationController.navigationBar setHidden:YES];
    
    
    self.createAccountButton.layer.borderWidth = 1;
    self.createAccountButton.layer.borderColor = [UIColor hx_colorWithHexString:@"160F41"].CGColor;
}


- (IBAction)loginButtonAction:(id)sender {
    
    [[ServerManager sharedManager] postLoginWithPhone:self.phoneNoTextField.text password:self.passwordTextField.text completion:^(BOOL success) {
        
        if (success) {
            
            NSLog(@"success");
            
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults] setValue:self.phoneNoTextField.text forKey:@"phoneNo"];
            [[NSUserDefaults standardUserDefaults] setValue:self.passwordTextField.text forKey:@"passWord"];

            
            UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
            KYDrawerController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"KYDrawerController"];
            UIWindow *window = [UIApplication sharedApplication].delegate.window;
            window.rootViewController = destinationVC;

        }
        
        
    }];
    
}



@end
