//
//  HomeViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "HomeViewController.h"
#import "KYDrawerController.h"
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
#import "UserAccount.h"
#import "SocketRocket.h"


@interface HomeViewController ()<MFMessageComposeViewControllerDelegate,CLLocationManagerDelegate,SRWebSocketDelegate>{
    
    CLLocationManager *locationManager;
    
    CLLocationCoordinate2D currentLocation;
    
    Reachability *networkReachability;
    
    SRWebSocket *_webSocket;
    
    NSString *phoneNo;
    
    BOOL isRegistaringSocket;
    
    NSTimer* timerForUserPosition;
}
@property (nonatomic, readwrite) BOOL isNetworkAvailable;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getLocation];
    
    NSLog(@"[UserAccount  sharedInstance]accessToken %@",[UserAccount sharedInstance].accessToken);
    
    phoneNo = [[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNo"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocationtoServer:) name:@"locationUpdate" object:nil];
    
    
    [[ServerManager sharedManager] postLoginWithPhone:phoneNo password:[[NSUserDefaults standardUserDefaults] objectForKey:@"passWord"] completion:^(BOOL success) {
        
        if (success) {
            
            NSLog(@"success.........");
        }
        
    }];

   
}
- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)updateLocationtoServer:(NSNotification*) notification{
    
    NSDictionary *userInfo = notification.userInfo;
    
    NSLog(@"notification userInfo %@",userInfo);
    
    
    NSDictionary*temp = @{@"path": @"/updateVictimLocation",@"number": phoneNo,@"latitude": [userInfo objectForKey:@"latitude"],@"longitude": [userInfo objectForKey:@"longitude"]};
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:temp
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    
    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"tempString %@",newStr);
    
//    isRegistaringSocket = NO;
//
    [_webSocket sendString:newStr error:NULL];

}

-(void)getLocation{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}

//--------locationmanager delegate to get current location-------------//
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    
    CLLocation *currentPostion=locations.lastObject;
    
    //CLLocation *currentPostion=locations.lastObject;
    currentLocation.latitude=currentPostion.coordinate.latitude;
    currentLocation.longitude=currentPostion.coordinate.longitude;
    
    NSLog(@"Current Location = %f, %f",currentLocation.latitude,currentLocation.longitude);
    
     [manager stopUpdatingLocation];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)menuButtonAction:(id)sender {
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

- (IBAction)getHelpButtonAction:(id)sender {
    
    if ([self checkForNetworkAvailability])  {
        
        NSLog(@"...... net");
        
        _webSocket.delegate = nil;
        
        [_webSocket close];
        
        
        
        NSString *socketString = [NSString stringWithFormat:@"ws://104.192.5.213/victim?path=/openVictimSocket&number=%@",phoneNo];
        
        _webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:socketString]];
        
        _webSocket.delegate = self;
        
        
        [_webSocket open];

        
    }else{
        
        NSLog(@"No net");
        
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            return;
        }
        
    
        NSArray *recipents = @[@"17722667147"];
        NSString *message = [NSString stringWithFormat:@"[SULTANA-SOS]\nLatitude %f\nLongitude %f",currentLocation.latitude,currentLocation.longitude];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
        
    }
    
//    if (timerForUserPosition) {
//
//        timerForUserPosition = nil;
//        [timerForUserPosition invalidate];
//    }
//    timerForUserPosition =  [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(getUserCurrentLocation) userInfo:nil repeats:YES];
//
    
    
    self.overlayView.hidden = NO;
    
    [self getUserCurrentLocation];
    
}

///--------------------------------------
#pragma mark - SRWebSocketDelegate
///--------------------------------------

- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
{
    NSLog(@"Websocket Connected");
    //closeVictimSocket
    //close socket
    
//    NSDictionary*temp1 = @{@"path": @"/closeVictimSocket",@"number": phoneNo};
//
//    NSData* jsonData1 = [NSJSONSerialization dataWithJSONObject:temp1
//                                                       options:NSJSONWritingPrettyPrinted error:nil];
//
//
//    NSString* newStr1 = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
//
//    NSLog(@"tempString %@",newStr1);
//
//    [_webSocket sendString:newStr1 error:NULL];
    
    //...........

    
    NSDictionary*temp = @{@"path": @"/openVictimSocket",@"number": phoneNo};

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:temp
                                                       options:NSJSONWritingPrettyPrinted error:nil];

    
   NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"tempString %@",newStr);
    
    isRegistaringSocket = YES;
    
    [_webSocket sendString:newStr error:NULL];
    
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error;
{
    NSLog(@":( Websocket Failed With Error %@", error);
    
    self.title = @"Connection Failed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(nonnull NSString *)string
{
    NSLog(@"Received \"%@\"", string);
    
    if (isRegistaringSocket) {
        
        [self timerCalled];
    }
    
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
   if ([json objectForKey:@"respondent_contact_number"] != nil){
        
        self.firstNameLabel.text = [NSString stringWithFormat:@"%@",[json objectForKey:@"respondent_first_name"]];
       self.lastNameLabel.text = [NSString stringWithFormat:@"%@",[json objectForKey:@"respondent_last_name"]];
       self.genderLabel.text = [NSString stringWithFormat:@"%@",[json objectForKey:@"respondent_gender"]];
       self.designationLabel.text = [NSString stringWithFormat:@"%@",[json objectForKey:@"respondent_designation"]];
        
    }
    
   
    if ([json objectForKey:@"message"] != nil) {
        
        if ([[json objectForKey:@"message"] isEqualToString:@"Case Closed"]) {
            
            NSLog(@"Case Closed..............");
            
            [APP_DELEGATE.locationTracker startLocationTracking];
            [APP_DELEGATE.locationUpdateTimer invalidate];
            
            self.overlayView.hidden = YES;
            
        }
    }

    
//    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerCalled) userInfo:nil repeats:YES];
//    [[NSRunLoop mainRunLoop] addTimer: timer forMode:NSRunLoopCommonModes];

    
//
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean;
{
    NSLog(@"WebSocket closed reson %@",reason);
    self.title = @"Connection Closed! (see logs)";
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
    NSLog(@"WebSocket received pong");
}


-(void)timerCalled
{
    NSLog(@"Timer Called");
    
    NSString*lat =[NSString stringWithFormat:@"%f",currentLocation.latitude];
    NSString*lng =[NSString stringWithFormat:@"%f",currentLocation.longitude];


    NSDictionary*temp = @{@"path": @"/updateVictimLocation",@"number": phoneNo,@"latitude": lat,@"longitude": lng};

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:temp
                                                       options:NSJSONWritingPrettyPrinted error:nil];


    NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    NSLog(@"tempString %@",newStr);

    isRegistaringSocket = NO;
    
    [_webSocket sendString:newStr error:NULL];

}

-(void)getUserCurrentLocation{
    
    
    APP_DELEGATE.locationTracker = [[LocationTracker alloc]init];
    [APP_DELEGATE.locationTracker startLocationTracking];
    
    //Send the best location to server every 60 seconds
    //You may adjust the time interval depends on the need of your app.
    NSTimeInterval time = 60.0;
    APP_DELEGATE.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
    
}

-(void)updateLocation {
    NSLog(@"updateLocation");
    
    [APP_DELEGATE.locationTracker updateLocationToServer];
}




#pragma mark - Network Reachability
- (BOOL)checkForNetworkAvailability{
    networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if ((networkStatus != ReachableViaWiFi) && (networkStatus != ReachableViaWWAN)) {
        self.isNetworkAvailable = FALSE;
    }else{
        self.isNetworkAvailable = TRUE;
    }
    
    return self.isNetworkAvailable;
}

@end
