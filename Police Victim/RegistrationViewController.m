//
//  RegistrationViewController.m
//  Police Victim
//
//  Created by Md.Ballal Hossen on 14/3/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import "RegistrationViewController.h"
#import "KYDrawerController.h"
#import "ServerManager.h"

@interface RegistrationViewController ()<IQDropDownTextFieldDelegate>{
    
    NSArray *dropDownArray;
}

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 
    self.contactRelationTextField.showDismissToolbar = YES;
    [self.contactRelationTextField setItemList:[NSArray arrayWithObjects:@"Father",@"Mother",@"Brother",@"Sister",@"Husband",@"Grandparent",@"Uncle",@"Aunt",@"Legal Guardian", nil]];
    [self.contactRelationTextField setItemListUI:[NSArray arrayWithObjects:@"Father",@"Mother",@"Brother",@"Sister",@"Husband",@"Grandparent",@"Uncle",@"Aunt",@"Legal Guardian", nil]];
    
    self.firstnameTestField.text = @"Sultana";
    self.lastNameTextField.text = @"Beg";
    self.ageTextField.text = @"25";
    self.occupationTextField.text = @"Student";
    self.contactNoTF.text = @"+8801684572006";
    self.permanentAddTF.text = @"32,malibagh,dhaka-1200";
    self.presentAddTF.text = @"32,malibagh,dhaka-1200";
    self.emarContactTF.text = @"+8801684572006";
    self.contactRelationTextField.text = @"Father";
    self.passwordTF.text=@"sultana123";

    
}

-(void)textField:(nonnull IQDropDownTextField*)textField didSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
}
-(BOOL)textField:(nonnull IQDropDownTextField*)textField canSelectItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return YES;
}

-(IQProposedSelection)textField:(nonnull IQDropDownTextField*)textField proposedSelectionModeForItem:(nullable NSString*)item
{
    NSLog(@"%@: %@",NSStringFromSelector(_cmd),item);
    return IQProposedSelectionBoth;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
    
    if (textField == self.firstnameTestField) {
        
        if (textField.text.length < 3) {
            
            NSLog(@"First name must have at least 3 carc.");
        }
        
    }else if (textField == self.lastNameTextField){
        
        if (textField.text.length < 3) {
            
            NSLog(@"Last name must have at least 3 carc.");
        }
    }else if (textField == self.ageTextField){
        
        if ([textField.text doubleValue] < 10) {
            
            NSLog(@"Age can not be less than 10");
        }
        
    }else if (textField == self.permanentAddTF){
        
        if ([textField.text doubleValue] < 10) {
            
            NSLog(@"address length should be gretter than 20");
        }
    }else if (textField == self.permanentAddTF){
        
        if ([textField.text doubleValue] < 10) {
            
            NSLog(@"address length should be gretter than 20");
        }
    }
    
   
    
}

-(void)doneClicked:(UIBarButtonItem*)button
{
    [self.view endEditing:YES];
    
    NSLog(@"textFieldTextPicker.selectedItem: %@", self.contactRelationTextField.selectedItem);
    
}

- (IBAction)registerButtonAction:(id)sender {
    
    NSMutableDictionary*temp = [[NSMutableDictionary alloc]init];
    
    [temp setObject:self.firstnameTestField.text forKey:@"first_name"];
    [temp setObject:self.lastNameTextField.text forKey:@"last_name"];
    [temp setObject:self.ageTextField.text forKey:@"age"];
    [temp setObject:self.occupationTextField.text forKey:@"occupation"];
    [temp setObject:self.contactNoTF.text forKey:@"contact_number"];
    [temp setObject:self.permanentAddTF.text forKey:@"permanent_address"];
    [temp setObject:self.presentAddTF.text forKey:@"present_address"];
    [temp setObject:self.emarContactTF.text forKey:@"emergency_contact"];
    [temp setObject:self.contactRelationTextField.text forKey:@"emergency_contact_relation"];
    [temp setObject:self.passwordTF.text forKey:@"password"];
    [temp setObject:self.contactNoTF.text forKey:@"username"];
    
   // NSLog(@"temp.........%@",temp);
    
    
    [[ServerManager sharedManager] postSignUpWithDictionary:temp completion:^(BOOL success, NSMutableDictionary *resultDataDic) {
        
        if (success) {
            
            NSLog(@"registration success %@",resultDataDic);
            
            UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:Nil];
            KYDrawerController *destinationVC = [homeStoryBoard instantiateViewControllerWithIdentifier:@"KYDrawerController"];
            [self.navigationController pushViewController:destinationVC animated:YES];
            
        }
        
        
    }];
    
    
 
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
