//
//  LoginViewController.h
//  Police Victim
//
//  Created by Md.Ballal Hossen on 1/4/19.
//  Copyright © 2019 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *phoneNoTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;


@end

NS_ASSUME_NONNULL_END
